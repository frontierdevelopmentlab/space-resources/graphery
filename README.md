# Graphery

Graph manipulation to create navigation graphs on on top of Mapstery, using networkx.

## Installation

In your python environment (conda: `source activate my_env`)

```
pip install graphery
```

## Usage

```
import graphery

nav_graph = graphery.
```

## For Developers - Install the Lastest Commit

In your python environment:

```
$ make build
$ pip uninstall graphery
$ pip install dist/graphery-X.Y.Z-py2.py3-none-any.whl
```

where `X.Y.Z` is the lastest version marked in `setup.py`.
